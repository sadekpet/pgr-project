PGR projekt
==========

Zde je odkaz na stránku s popisem projektu:
<https://cent.felk.cvut.cz/courses/PGR/archives/2019-2020/S-FEL/sadekpet/>

Spustitelné soubory release-windows.exe a release-linux jsou ve složce app.

Pro linux je třeba mít nainstalované následující knihovny: glut, assimp, DevIL, AntTweakBar, zlib, GLEW, X11, GLX
 
Pomocí premake se dá vygenerovat buď makefile, nebo visual studio projekt, avšak obojí by mělo být aktuální.